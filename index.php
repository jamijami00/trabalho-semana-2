<!DOCTYPE html>
<html lang="pt-br">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <style>
        @import url('https://fonts.googleapis.com/css2?family=Darker+Grotesque:wght@300;400;600;700&display=swap');
        </style>
    <link rel="stylesheet" href="css/style.css">
    <title>Adote um Lobinho</title>
</head>
<body>
    <header class="headerNav">
        <a href = "lista-de-lobinhos.html" a>Nossos Lobinhos</a>
        <a href = "index.html" a><img src="./assets/Logo.svg" width="100" height="100"></a> 
        <a href = "quemSomos.html" a>Quem Somos</a>
    </header>
    <main>
        <section class="bannerLobo centralizar">
            <h1>
                Adote um Lobinho
            </h1>
            <p class="loboSubtitulo centralizar">É claro que o consenso sobre a necessidade de qualificação apresenta tendências no sentido de aprovar a manutenção das regras de conduta normativas.</p>
        </section>
        <section class="caixa azul centralizar" id="caixaSobre">
            <h2>Sobre</h2>
            <p class="margemSobre">
                Não obstante, o surgimento do comércio virtual faz parte de um processo de gerenciamento do levantamento das variáveis envolvidas. Não obstante, o surgimento do comércio virtual faz parte de um processo de gerenciamento do levantamento das variáveis envolvidas.Não obstante, o surgimento do comércio virtual faz parte de um processo de gerenciamento do levantamento das variáveis envolvidas.Não obstante, o surgimento do comércio virtual faz parte de um processo de gerenciamento do levantamento das variáveis envolvidas.
            </p>
        </section>
        <section class="caixa cinza centralizar">
            <h2>Valores</h2>
            <div class="displayFlex">
                <div class="listaVertical">
                    <figure class="iconeValores"><img src="./assets/life-insurance.svg" height="150" width="112"></figure>
                    <h3>Proteção</h3>
                    <p class="pLista">
                        Assim mesmo, o desenvolvimento contínuo de distintas formas de atuação facilita a criação do sistema de participação geral.
                    </p>
                </div>
                <div class="listaVertical">
                    <figure class="iconeValores"><img src="./assets/care.svg" height="160" width="112"></figure>
                    <h3>Carinho</h3>
                    <p class="pLista">
                        Assim mesmo, o desenvolvimento contínuo de distintas formas de atuação facilita a criação do sistema de participação geral.
                    </p>
                </div>
                <div class="listaVertical">
                    <figure class="iconeValores"><img src="./assets/care.svg" height="160" width="112"></figure>
                    <h3>Companheirismo</h3>
                    <p class="pLista">
                        Assim mesmo, o desenvolvimento contínuo de distintas formas de atuação facilita a criação do sistema de participação geral.
                    </p>
                </div>
                <div class="listaVertical">
                    <figure class="iconeValores"><img src="./assets/care.svg" height="160" width="112"></figure>
                    <h3>Resgate</h3>
                    <p class="pLista">
                        Assim mesmo, o desenvolvimento contínuo de distintas formas de atuação facilita a criação do sistema de participação geral.
                    </p>
                </div>
            </div>
        </section>
        <section class="caixaVazia">
            <h2 class="centralizar">Lobos Exemplo</h2>
            <!-- Os lobos aparecem aqui -->
        </section>
    </main>
    <footer class="rodape">
        <div class="juntar">
            <div id="mapa"><iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3675.1968390018756!2d-43.13644248503438!3d-22.906109485012156!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x99817ee1756031%3A0xd1dcbde0df6f873c!2sAv.%20Milton%20Tavares%20de%20Souza%2C%20Niter%C3%B3i%20-%20RJ%2C%2024210-346!5e0!3m2!1spt-BR!2sbr!4v1632958642432!5m2!1spt-BR!2sbr" width="300" height="200" style="border:0;" allowfullscreen="" loading="lazy"></iframe></div>
            <div id="endereco">
                <div class="ladoDoMapa"><img class ="icon" src="assets/pin.png" alt="localiza" width="20" height="20">Av. Milton Tavares de Souza, s/n - Sala 115 B - Boa Viagem, Niterói - RJ, 24210-315</div>
                <div class="ladoDoMapa"><img class ="icon" src="assets/telephone.png" alt="telefone" width="20" height="20">(99)99999-9999</div>
                <div class="ladoDoMapa"><img class ="icon" src="assets/mail.png" alt="mail" width="20" height="20">salve-lobos@lobINhos.com</div>
                <br>
                <a href="quemSomos.html" class="quemSomos">Quem Somos</a>
            </div>
        </div>
        
        <!-- Isso aqui em cima é um botão! -->
        <p class="patinha"> Desenvolvido com 
            <img src="assets/paws.svg" alt="pataDeLobo">
        </p> 
    </footer>
</body>
<script src="./js/pagina-home.js"></script>
</html>