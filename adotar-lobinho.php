<!DOCTYPE html>
<html lang="pt-BR">
  <head>
    <meta charset="UTF-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <style>
      @import url('https://fonts.googleapis.com/css2?family=Darker+Grotesque:wght@300;400;600;700&display=swap');
    </style>
    <link rel="stylesheet" href="css/style.css" />
    <title>Adote um Lobinho</title>
  </head>
  <body>
    <header class="headerNav">
        <a href = "lista-de-lobinhos.html" a>Nossos Lobinhos</a>
        <a href = "index.html" a><img src="./assets/Logo.svg" width="100" height="100"></a> 
        <a href = "quemSomos.html" a>Quem Somos</a>
    </header>
    <main>
        <main>
            <div class="container">
                <div class = "conteudo">
                    <div class = "cabecalho">
                        <figure><img class = "redondo" src = "assets/lobo.png" width= "324" height= "216" ></figure>
                        <div class = "tituloESub">
                            <h1 class = "labelCampo" id = "nomeLobo">Adote o(a) NomeDoLobo</h1>
                            <p class = "labelCampo" id = "idLobo">ID:314159</p>
                        </div>
                    </div>
                    <div class= "divDoForm">
                        <form class= "forms">
                            <div class = "nomeIdade">
                                <div>
                                    <p class ="labelCampo">Seu nome:</p><br>
                                    <input type = "text" class = "inputFormsNome" placeholder=""><br>
                                </div>
                                <div>
                                    <p class = "labelCampo">Idade:</p><br>
                                    <input type = "number" class = "inputFormsIdade" placeholder=""><br>
                                </div>
                            </div>
                            <div>
                                <p class = "labelCampo" id = "emailAdota">E-mail:</p><br>
                                <input type=" text" class = "inputFormsMail" placeholder=""><br>
                            </div>
                            <div class ="divBotao"><button class ="azulBotao"><p>Adotar</p></button></div>

                        </form>

                    </div>
                </div>
              </form>
            </div>
          </div>
        </div>
      </main>
    </main>
    <footer class="rodape">
      <div class="juntar">
        <div id="mapa">
          <iframe
            src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3675.1968390018756!2d-43.13644248503438!3d-22.906109485012156!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x99817ee1756031%3A0xd1dcbde0df6f873c!2sAv.%20Milton%20Tavares%20de%20Souza%2C%20Niter%C3%B3i%20-%20RJ%2C%2024210-346!5e0!3m2!1spt-BR!2sbr!4v1632958642432!5m2!1spt-BR!2sbr"
            width="300"
            height="200"
            style="border: 0"
            allowfullscreen=""
            loading="lazy"
          ></iframe>
        </div>
        <div id="endereco">
          <div class="ladoDoMapa">
            <img
              class="icon"
              src="assets/pin.png"
              alt="localiza"
              width="20"
              height="20"
            />Av. Milton Tavares de Souza, s/n - Sala 115 B - Boa Viagem,
            Niterói - RJ, 24210-315
          </div>
          <div class="ladoDoMapa">
            <img
              class="icon"
              src="assets/telephone.png"
              alt="telefone"
              width="20"
              height="20"
            />(99)99999-9999
          </div>
          <div class="ladoDoMapa">
            <img
              class="icon"
              src="assets/mail.png"
              alt="mail"
              width="20"
              height="20"
            />salve-lobos@lobINhos.com
          </div>
          <br />
          <a href="quemSomos.html" class="quemSomos">Quem Somos</a>
        </div>
      </div>

      <!-- Isso aqui em cima é um botão! -->
      <p class="patinha">
        Desenvolvido com
        <img src="assets/paws.svg" alt="pataDeLobo" />
      </p>
    </footer>
  </body>
</html>
